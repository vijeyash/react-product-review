import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Products from "./Products";
const App = () => (
  <div>
    <Products />
  </div>
);
export default App;
