import React from "react";
import Rating from "./Rating";
import { Stack } from "react-bootstrap";
import Image from "react-bootstrap/Image";
const Product = (props) => {
  return (
    <Stack gap={3}>
      <div className="bg-light border">
        <Image
          width={50}
          height={50}
          className="rounded"
          src={props.data.imageUrl}
          alt="Image"
        />

        <h5>{props.data.productName}</h5>
        {props.data.releasedDate}
        <Rating
          rating={props.data.rating}
          numOfReviews={props.data.numOfReviews}
        />
        <p>{props.data.description}</p>
      </div>
    </Stack>
  );
};
export default Product;
