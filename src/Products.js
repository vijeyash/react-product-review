import React from "react";
import Product from "./Product";
import List from "./list";

const Products = () => {
  const products = List();
  const listProducts = products.map((product) => (
    <Product key={product.productName} data={product} />
  ));

  return (
    <div>
      <ol>{listProducts}</ol>
    </div>
  );
};

export default Products;
